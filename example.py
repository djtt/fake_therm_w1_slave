#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Example fake temperature sensors for testing pythonscript without raspberry pi using temperature sensors
# By Ted van Roon
# v0.1

import os
import time, datetime
from tempSensor.tempSensor import tempSensor

outputDir = "" # define a custom folder, otherwise a default output folder relative to this file will be chosen.
numberOfSensors = 1 # define number of sensors to be generated
minTemp = 17000 # define min temperature for the sensors Must be an integer with 5 digits
maxTemp = 19000 # define max temperature for the sensors Must be an integer with 5 digits

# make outputdir
if outputDir == "":
    basePath = os.path.dirname(__file__)
    outputDir = os.path.join(basePath, 'output')

def main():
    todayLastTime = ""
    # create dict for sensors
    sensors = {}

    # instantaniate number of sensors
    for i in range(numberOfSensors):
        sensors[i] = sensors.get(i, tempSensor(minTemp, maxTemp, outputDir, i))
    
    while True:
        now = datetime.datetime.now()
        todayTime = now.strftime("%H:%M:%S")
        if todayTime != todayLastTime:
            todayLastTime = todayTime
            # run sensors
            for x in range(numberOfSensors):
                sensors[x].makeW1SlaveFile()
                pass

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass